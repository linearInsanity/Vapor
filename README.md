# Vapor

Vapor is a TUI+GUI application used as a market for digital content. Unlike most
online markets, vapor provides somewhat of a decentralized system where users can
add repositories and make seperate purchases through different sources, all while
keeping tabs on one consistent database.

### Accessing Bazzars

An online Vapor Repository is formally known as a Bazzar. Bazzars store a tab of
all of the hosted content as well as the contents source code (should the author
choose to provide it) in a single file called the *Catalog*. To access different
Bazzars, a user must enter the location of their Catalog. Using the catalog, the
client tabulates all of the content into a database with numerous tags including,
but not limited to: price, license, source code available?, remote location,
et cetera. This allows the user to run querys using commands such as
`vapor search <query>` to search for a package in all of their Bazzars. 

### Installing Content

The vapor client keeps track of the packages installed and can install, upgrade,
remove, build, and clean.

#### Free Content

Free content can be easily managed due to the fact that it requires no authentication
to get from the server. All Free Content installations consist of retrieving the
files off of the server and installing to the appropriate directory.

#### Paid Content

With Paid Content, the vapor client requests the given `payment method` which is
usually a webpage where the payment takes place. Once the payment is done, the
Vapor server will generate a *Serial*. The Serial must be added to the client's keystore in
order to retrieve the purchased application. The serial is added to the client
by using the command `vapor keystore add <package> <serial>`. When transfering to
another system, use the command `vapor keystore export <path-to-export>` in order
to export the keystore and use `vapor keystore import <file-to-import>` to import
the keystore.